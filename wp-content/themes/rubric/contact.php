<?php

/*
* Template Name: Contact
*/

get_header();

?>


<main id="contact">


  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="content container">

      <h1><?php echo get_the_title(); ?></h1>


    </div>

    <div class="blue">

      <div class="container">

        <div class="row">

          <div class="col-md-6">

            <?php the_content(); ?>

            <div id="contact-details">

              <span>
                <a href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a>
              </span>

              <span>
                <a href="mailto:<?php the_field('email_address', 'option'); ?>"><?php the_field('email_address', 'option'); ?></a>
              </span>

              <div class="social-icons">

                <?php if( have_rows('social_links', 'option') ):
                while( have_rows('social_links', 'option') ): the_row();
                  // vars
                  $socialurl = get_sub_field('social_url');
                  $socialicon = get_sub_field('social_icon');
                  ?>

                  <span class="social">
                    <a href="<?php echo esc_url( $socialurl ); ?>" target="_blank">
                      <img src="<?php echo $socialicon['url']; ?>" alt="<?php echo $socialicon['alt'] ?>" />
                      <p><?php the_sub_field('social_title'); ?></p>
                    </a>
                  </span>

                <?php endwhile; ?>
                <?php endif; ?>

              </div>

            </div>

          </div>

          <div class="col-md-6">

            <?php echo do_shortcode('[wpforms id="265"]');?>

          </div>

        </div>

      </div>

    </div>

  <?php endwhile; endif; ?>

</main>

<?php get_footer(); ?>
