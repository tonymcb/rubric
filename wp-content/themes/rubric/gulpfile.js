var gulp = require('gulp');
// Requires the gulp-sass plugin
var postcss = require('gulp-postcss');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass')(require('sass'));


gulp.task('sass', function () {
  return gulp
    .src('src/scss/**/*.scss')
    .pipe(sass({
    }).on('error', sass.logError))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./')); // output to theme root
});

gulp.task('watch', function(){
  browserSync.init({
    port: 8080,
    proxy: {
    target: "http://rubric/",
    }
  });
  gulp.watch('src/scss/**/*.scss', gulp.series('sass'));
  gulp.watch('./style.css').on('change', browserSync.reload);
  gulp.watch('./*.php').on('change', browserSync.reload);
  // Other watchers

})
