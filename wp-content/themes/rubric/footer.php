</div>

<div id="cta" class="green">

  <div class="container">
    <div class="row">
      <div class="col-12">
        <div>
          <h3>Let's work together</h3>
          <a href="/contact" class="btn">Get in touch</a>
        </div>
      </div>
    </div>
  </div>

</div>

<footer id="footer">

  <div class="container">

    <div id="footer-logo"></div>

    <div id="footer-contact">

      <div id="footer-contact-details">

        <span>
          <a href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a>
        </span>

        <span>
          <a href="mailto:<?php the_field('email_address', 'option'); ?>"><?php the_field('email_address', 'option'); ?></a>
        </span>

        <div class="social-icons">

          <?php if( have_rows('social_links', 'option') ):
          while( have_rows('social_links', 'option') ): the_row();
            // vars
            $socialurl = get_sub_field('social_url');
            $socialicon = get_sub_field('social_icon');
            ?>

            <span class="social">
              <a href="<?php echo esc_url( $socialurl ); ?>" target="_blank">
                <img src="<?php echo $socialicon['url']; ?>" alt="<?php echo $socialicon['alt'] ?>" />
                <p><?php the_sub_field('social_title'); ?></p>
              </a>
            </span>

          <?php endwhile; ?>
          <?php endif; ?>

        </div>

      </div>


    </div>

  </div>

  <div id="copyright">
  &copy; <?php echo esc_html( date_i18n( __( 'Y', 'blankslate' ) ) ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?>
  </div>

</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
