<?php get_header(); ?>
<main id="content">
<header class="header">
<h1 class="entry-title"><?php single_term_title(); ?></h1>
<div class="archive-meta"><?php if ( '' != the_archive_description() ) { echo esc_html( the_archive_description() ); } ?></div>
</header>
<h1>This is the clients page</h1>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <div class="col-12">

    <?php

  $args = array(
   'post_type' => 'clients',
   'posts_per_page' => -1, // limit the number of posts if you like to
   'orderby' => 'title',
   'order' => 'ASC'
  );

  $custom_query = new WP_Query($args);

  if ($custom_query->have_posts()) : while($custom_query->have_posts()) : $custom_query->the_post(); ?>
    <h1><?php the_title(); ?></h1>
    <div class="col-6">
      <?php
      $image = get_field('client_logo');
      if( !empty( $image ) ): ?>
        <span class="icon"><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" /></span>
      <?php endif; ?>
    </div>

    <div class="col-6">
      <?php the_field('client_content'); ?>
    </div>


    <?php the_content();?>
  <?php endwhile; else : ?>
    <p>No posts</p>
  <?php endif; wp_reset_postdata(); ?>


  </div>

<?php endwhile; endif; ?>
<?php get_template_part( 'nav', 'below' ); ?>
</main>
<?php get_footer(); ?>
