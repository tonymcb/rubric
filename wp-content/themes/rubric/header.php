<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <title>
  <?php is_front_page() ? bloginfo('name') : wp_title(''); ?> |
  <?php is_front_page() ? bloginfo('description') : bloginfo('name'); ?></title>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0" />
  <?php wp_head(); ?>
  <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
  <link rel="manifest" href="/favicons/site.webmanifest">
  <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#D62929">
  <link rel="shortcut icon" href="/favicons/favicon.ico">

  <link rel="stylesheet" href="https://use.typekit.net/rls0wop.css">

  <meta name="msapplication-TileColor" content="#d62929">
  <meta name="msapplication-config" content="/favicons/browserconfig.xml">
  <meta name="theme-color" content="#d62929">
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">

  <header id="header">


    <div id="nav" class="container">

      <div id="logo-container"><a href="/" id="logo"></a></div>

      <nav id="menu">

        <div id="nav-icon">
          <div></div>
        </div>

        <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>

      </nav>

    </div>

  </header>

<div id="container">
