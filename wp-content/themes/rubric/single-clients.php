<?php
/*
Template Name: Clients
Template Post Type: post, clients
*/
?>
<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<main id="content">

  <div id="intro" class="container">
    <h1><?php the_title(); ?></h1>

    <div>
      <?php the_field('client_introduction'); ?>
    </div>

  </div>

  <div class="container content">

    <div class="row">

      <div class="col-md-6">
        <?php
        $image = get_field('client_logo');
        if( !empty( $image ) ): ?>
          <div class="icon">
            <div>
              <div>
                <div>
                  <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      </div>

      <div class="col-md-6">
        <?php the_field('client_content'); ?>
      </div>

    </div>

  </div>

  <div id="testimonials" class="row">

    <div class="container">

      <?php
      $testimonial = get_field('client_testimonial');
      if( $testimonial ): ?>

        <div class="testimonial">

          <div>

            <span class="user-image">
              <?php
              $image = get_field('testimonial_image',$testimonial);
              if( !empty( $image ) ): ?>
                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
              <?php endif; ?>
            </span>

            <div>
              <div>
                <div class="testimonial-content"><?php echo get_field( 'testimonial_content',  $testimonial ); ?></div>
                <div class="testimonial-name"><?php echo get_field( 'testimonial_name',  $testimonial ); ?></div>
                <div class="testimonial-job"><?php echo get_field( 'testimonial_job_position',  $testimonial ); ?></div>
                <div class="testimonial-company"><?php echo get_field( 'testimonial_company',  $testimonial ); ?></div>
              </div>
            </div>

          </div>

        </div>

      <?php endif; ?>

    </div>


  </div>

  <div id="services" class="blue">

    <div class="container">

      <div class="row">

        <div class="col-md-6">

          <h2>Services provided</h2>

        </div>

        <div class="col-md-6">

          <?php
          $posts = get_field('client_services');

          if( $posts ): ?>
                <ul>
                  <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>

                    <li>

                      <a href="<?php echo get_permalink( $p->ID ); ?>">

                        <span class="services-icon">
                          <?php
                          $image = get_field('service_icon',$p->ID);
                          if( !empty( $image ) ): ?>
                            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                          <?php endif; ?>
                        </span>

                        <div>
                          <h3><?php echo get_the_title( $p->ID ); ?></h3>
                        </div>

                      </a>

                    </li>
                <?php endforeach; ?>
                </ul>
                <?php
                // Reset the global post object so that the rest of the page works correctly.
                wp_reset_postdata(); ?>
            <?php endif; ?>

          </div>

      </div>

    </div>

  </div>

</main>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
