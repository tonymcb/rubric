<?php

/*
* Template Name: About
*/

get_header();

?>


<main id="about">


  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="content container">

      <div class="row">

        <div class="col-md-6">

          <h1>Our <span>Journey</span><h1>

        </div>

        <div class="col-md-6">

          <?php the_content(); ?>

        </div>

      </div>


    </div>

    <div class="blue">

      <div class="container">

        <h2>Our values</h2>

        <?php if( have_rows('values') ): ?>
            <ul class="values">
            <?php while( have_rows('values') ): the_row();
                $image = get_sub_field('value_icon');
                ?>
                <li>
                  <div>
                    <div class="values-icon"><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" /></div>
                    <h5><?php the_sub_field('value_title'); ?></h5>
                    <p><?php the_sub_field('value_content'); ?></p>
                  </div>
                </li>
            <?php endwhile; ?>
            </ul>
        <?php endif; ?>

      </div>

    </div>

    <div class="container">

      <section id="our-team" class="row">

        <div class="col-md-6">
          <?php
          $image = get_field('team_image');
          if( !empty( $image ) ): ?>
              <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
          <?php endif; ?>
        </div>

        <div class="col-md-6 editor">
          <?php the_field('team_content'); ?>
        </div>

      </section>

    </div>

  <?php endwhile; endif; ?>

</main>

<?php get_footer(); ?>
