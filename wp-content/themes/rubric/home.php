<?php
/*
* Template Name: Home
*/
get_header();

?>


<main id="home">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div id="banner">

      <div class="container">

        <div class="row">

          <div class="col-12">

        <h1>
          Revolutionise
          <span>Your IT</span>
        </h1>

        <div id="banner-content">

          <div>
            <?php the_field('banner_content'); ?>
          </div>

          <?php if( have_rows('banner_logos') ): ?>

            <div id="banner-logos">

              <ul>

              <?php while( have_rows('banner_logos') ): the_row();
                $logo = get_sub_field('banner_logo');
                ?>

                <li>
                    <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>"/>
                </li>

              <?php endwhile; ?>

              </ul>

            </div>

          <?php endif; ?>

          </div>

          </div>

        </div>

      </div>

      <div id="video_wrapper">

        <div>

          <video class="video" width="100%" height="100%" preload="none" autoplay loop autobuffer muted playsinline>
            <source data-src="<?php echo $video['url']; ?>" src="<?php echo get_bloginfo( 'template_directory' ); ?>/src/video/banner.mp4" type="video/mp4">
          </video>

        </div>

      </div>

    </div>


    <div class="content container">

      <section id="what-we-do" class="row">

        <div class="col-12">

          <h2>What we do</h2>

          <?php echo do_shortcode("[reblex id='112']"); ?>

        </div>

      </section>

      <section id="how-we-work" class="row alignfull">

        <div class="col-md-6">
          <div>
            <?php the_field('how_we_work_left_block'); ?>
          </div>
        </div>

        <?php if( have_rows('how_we_work_right_block') ): ?>

          <div class="col-md-6">

            <ul>

            <?php while( have_rows('how_we_work_right_block') ): the_row();?>

                <?php if( have_rows('steps') ): ?>

                  <?php while( have_rows('steps') ): the_row();?>
                    <li>
                      <div>
                        <h3><?php echo the_sub_field('step_title'); ?></h3>
                        <p><?php echo the_sub_field('step_content'); ?></p>
                      </div>
                    </li>
                  <?php endwhile; ?>

                <?php endif; ?>

            <?php endwhile; ?>

            </ul>

          </div>

        <?php endif; ?>

      </section>

      <section id="about-us" class="row">

        <div class="col-md-6">
          <?php
          $image = get_field('about_image');
          if( !empty( $image ) ): ?>
              <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
          <?php endif; ?>
        </div>

        <div class="col-md-6 editor">
          <?php the_field('about_content'); ?>
        </div>

      </section>

      <section id="testimonials" class="alignfull">

        <div class="container">

          <div class="row">

            <div class="col-12">

              <div id="<?php echo esc_attr($id); ?>" class="testimonials-block">

                <?php
                  $posts = get_field('testimonials_home');

                  if( $posts ): ?>
                  <ul>
                  <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>

                    <li class="testimonial">

                      <div>

                        <span class="user-image">
                          <?php
                          $image = get_field('testimonial_image',$p->ID);
                          if( !empty( $image ) ): ?>
                            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                          <?php endif; ?>
                        </span>

                        <div>
                          <div>
                            <div class="testimonial-content"><?php echo get_field( 'testimonial_content',  $p->ID ); ?></div>
                            <div class="testimonial-name"><?php echo get_field( 'testimonial_name',  $p->ID ); ?></div>
                            <div class="testimonial-job"><?php echo get_field( 'testimonial_job_position',  $p->ID ); ?></div>
                            <div class="testimonial-company"><?php echo get_field( 'testimonial_company',  $p->ID ); ?></div>
                          </div>
                        </div>

                      </div>


                    </li>
                  <?php endforeach; ?>
                  </ul>
                  <?php endif; ?>


              </div>

            </div>

          </div>

        </div>

      </section>

      <section id="who-we-work-with" class="row">

        <div class="col-12">

        <?php the_field('who_title'); ?>

        <?php if( have_rows('who_we_work_with') ): ?>

            <ul>

            <?php while( have_rows('who_we_work_with') ): the_row();?>

              <li>
                <span></span>
                <div>
                  <p><?php echo the_sub_field('who_item'); ?></p>
                </div>
              </li>

            <?php endwhile; ?>

            <li>
              <h3>Yes<span>?</span></h3>
              <h4>Then we work with you!</h4>
            </li>

            </ul>

        </div>

        <?php endif; ?>

      </section>

      <section id="why-rubric-solutions" class="row alignfull">

        <div class="col-md-6">

          <?php the_field('why_content'); ?>

        </div>

          <?php if( have_rows('why_reasons') ): ?>

            <div class="col-md-6">

              <ul>

              <?php while( have_rows('why_reasons') ): the_row();?>

                  <?php if( have_rows('why_item') ): ?>

                    <?php while( have_rows('why_item') ): the_row();?>
                      <li>
                        <div>
                          <?php
                          $image = get_sub_field('why_icon');
                          if( !empty( $image ) ): ?>
                              <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                          <?php endif; ?>
                          <div>
                            <h3><?php echo the_sub_field('why_title'); ?></h3>
                            <p><?php echo the_sub_field('why_content'); ?></p>
                          </div>
                        </div>
                      </li>
                    <?php endwhile; ?>

                  <?php endif; ?>

              <?php endwhile; ?>

              </ul>

            </div>

          <?php endif; ?>

      </section>

      <?php the_content(); ?>

    </div>


  <?php endwhile; endif; ?>

</main>

<?php get_footer(); ?>
