<?php
add_action( 'after_setup_theme', 'blank_setup' );
function blank_setup() {
load_theme_textdomain( 'blank', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'search-form' ) );
global $content_width;
if ( ! isset( $content_width ) ) { $content_width = 1920; }
register_nav_menus( array( 'main-menu' => esc_html__( 'Main Menu', 'blank' ) ) );
}
add_action( 'wp_enqueue_scripts', 'blank_load_scripts' );
function blank_load_scripts() {
wp_enqueue_style( 'blank-style', get_stylesheet_uri() );
wp_enqueue_script( 'jquery' );
}
function my_theme_scripts_function() {

  wp_enqueue_script( 'myscript', get_template_directory_uri() . '/src/js/custom.js');
  wp_enqueue_script( 'slick', get_template_directory_uri() . '/src/js/slick.js');
  wp_enqueue_script( 'waypoint', get_template_directory_uri() . '/src/js/waypoint.js');

}

add_action('wp_enqueue_scripts','my_theme_scripts_function');

function fit() {

  wp_enqueue_script( 'fit', get_template_directory_uri() . '/src/js/jquery.fittext.js');

}

add_action('wp_enqueue_scripts','fit');

function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

add_theme_support( 'wp-block-styles' );
add_theme_support( 'align-wide' );
add_theme_support( 'editor-styles' );
add_editor_style( 'style-editor.css' );

add_action( 'wp_footer', 'blank_footer_scripts' );
function blank_footer_scripts() {
?>
<script>
jQuery(document).ready(function ($) {
var deviceAgent = navigator.userAgent.toLowerCase();
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
$("html").addClass("ios");
$("html").addClass("mobile");
}
if (navigator.userAgent.search("MSIE") >= 0) {
$("html").addClass("ie");
}
else if (navigator.userAgent.search("Chrome") >= 0) {
$("html").addClass("chrome");
}
else if (navigator.userAgent.search("Firefox") >= 0) {
$("html").addClass("firefox");
}
else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
$("html").addClass("safari");
}
else if (navigator.userAgent.search("Opera") >= 0) {
$("html").addClass("opera");
}
});
</script>
<?php
}
add_filter( 'document_title_separator', 'blank_document_title_separator' );
function blank_document_title_separator( $sep ) {
$sep = '|';
return $sep;
}
add_filter( 'the_title', 'blank_title' );
function blank_title( $title ) {
if ( $title == '' ) {
return '...';
} else {
return $title;
}
}
add_filter( 'the_content_more_link', 'blank_read_more_link' );
function blank_read_more_link() {
if ( ! is_admin() ) {
return ' <a href="' . esc_url( get_permalink() ) . '" class="more-link">...</a>';
}
}
add_filter( 'excerpt_more', 'blank_excerpt_read_more_link' );
function blank_excerpt_read_more_link( $more ) {
if ( ! is_admin() ) {
global $post;
return ' <a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="more-link">...</a>';
}
}
add_filter( 'intermediate_image_sizes_advanced', 'blank_image_insert_override' );
function blank_image_insert_override( $sizes ) {
unset( $sizes['medium_large'] );
return $sizes;
}
add_action( 'widgets_init', 'blank_widgets_init' );
function blank_widgets_init() {
register_sidebar( array(
'name' => esc_html__( 'Sidebar Widget Area', 'blank' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => '</li>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
add_action( 'wp_head', 'blank_pingback_header' );
function blank_pingback_header() {
if ( is_singular() && pings_open() ) {
printf( '<link rel="pingback" href="%s" />' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
}
}
add_action( 'comment_form_before', 'blank_enqueue_comment_reply_script' );
function blank_enqueue_comment_reply_script() {
if ( get_option( 'thread_comments' ) ) {
wp_enqueue_script( 'comment-reply' );
}
}
function blank_custom_pings( $comment ) {
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'blank_comment_count', 0 );
function blank_comment_count( $count ) {
if ( ! is_admin() ) {
global $id;
$get_comments = get_comments( 'status=approve&post_id=' . $id );
$comments_by_type = separate_comments( $get_comments );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}

function my_acf_block_render_callback( $block ) {

	$slug = str_replace('acf/', '', $block['name']);

	if( file_exists( get_theme_file_path("/blocks/{$slug}.php") ) ) {
		include( get_theme_file_path("/blocks/{$slug}.php") );
	}

}

function register_my_menu() {
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_my_menu' );


/*==================
  REGISTER ACF BLOCKS
==================*/

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
    'page_title' 	=> 'Footer',
    'menu_title'	=> 'Footer',
    'menu_slug' 	=> 'footer',
    'capability'	=> 'edit_posts',
    'redirect'		=> false
  ));

}

/*==================
  BLOCKS
==================*/


function register_acf_block_types() {

  // Services
  acf_register_block_type(array(
    'name'              => 'services',
    'title'             => __('Services'),
    'description'       => __('List of services'),
    'render_template'   => 'partials/blocks/services.php',
		'category'          => 'layout',
		'mode' 							=> 'edit',
		'icon'              => 'editor-ul',
		'supports'					=> array(
			'align' => array( 'full', 'wide' ),
		),
    'keywords'          => array( 'services','block' ),
  ));

  // Testimonials
  acf_register_block_type(array(
    'name'              => 'testimonials',
    'title'             => __('Testimonials'),
    'description'       => __('List of testimonials'),
    'render_template'   => 'partials/blocks/testimonials.php',
		'category'          => 'layout',
		'mode' 							=> 'edit',
		'icon'              => 'editor-ul',
		'supports'					=> array(
			'align' => array( 'full', 'wide' ),
		),
    'keywords'          => array( 'testimonials','block' ),
  ));

}




if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
    'page_title' 	=> 'Contact details',
    'menu_title'	=> 'Contact details',
    'menu_slug' 	=> 'Contact details',
    'capability'	=> 'edit_posts',
    'redirect'		=> false
  ));

}


// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
  add_action('acf/init', 'register_acf_block_types');
}


/*==================
  ADD SERVICES
==================*/

function services() {
  $labels = array(
    'name'               => _x( 'Services', 'post type general name' ),
    'singular_name'      => _x( 'Service', 'post type singular name' ),
    'add_new'            => _x( 'Add Service', 'desktop' ),
    'add_new_item'       => __( 'Add New Service' ),
    'edit_item'          => __( 'Edit Service' ),
    'new_item'           => __( 'New Service' ),
    'all_items'          => __( 'All Services' ),
    'view_item'          => __( 'View Service' ),
    'search_items'       => __( 'Search Services' ),
    'not_found'          => __( 'No Services found' ),
    'not_found_in_trash' => __( 'No Services found in the Trash' ),
    'menu_name'          => 'Services'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Services',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'thumbnail', 'excerpt'),
    'has_archive'   => false,
		'menu_icon'     => 'dashicons-desktop',
  );
  register_post_type( 'services', $args );
}

add_action( 'init', 'services' );



/*==================
  ADD TESTIMONIALS
==================*/

function testimonials() {
  $labels = array(
    'name'               => _x( 'Testimonials', 'post type general name' ),
    'singular_name'      => _x( 'Testimonial', 'post type singular name' ),
    'add_new'            => _x( 'Add Testimonial', 'desktop' ),
    'add_new_item'       => __( 'Add New Testimonial' ),
    'edit_item'          => __( 'Edit Testimonial' ),
    'new_item'           => __( 'New Testimonial' ),
    'all_items'          => __( 'All Testimonials' ),
    'view_item'          => __( 'View Testimonial' ),
    'search_items'       => __( 'Search Testimonials' ),
    'not_found'          => __( 'No Testimonials found' ),
    'not_found_in_trash' => __( 'No Testimonials found in the Trash' ),
    'menu_name'          => 'Testimonials'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Testimonials',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'thumbnail'),
    'has_archive'   => false,
		'menu_icon'     => 'dashicons-format-quote',
  );
  register_post_type( 'testimonials', $args );
}

add_action( 'init', 'testimonials' );



/*==================
  ADD CLIENTS
==================*/

function clients() {
  $labels = array(
    'name'               => _x( 'Clients', 'post type general name' ),
    'singular_name'      => _x( 'Client', 'post type singular name' ),
    'add_new'            => _x( 'Add Client', 'desktop' ),
    'add_new_item'       => __( 'Add New Client' ),
    'edit_item'          => __( 'Edit Client' ),
    'new_item'           => __( 'New Client' ),
    'all_items'          => __( 'All Clients' ),
    'view_item'          => __( 'View Client' ),
    'search_items'       => __( 'Search Clients' ),
    'not_found'          => __( 'No Clients found' ),
    'not_found_in_trash' => __( 'No Clients found in the Trash' ),
    'menu_name'          => 'Clients'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Clients',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'thumbnail'),
    'has_archive'   => false,
		'menu_icon'     => 'dashicons-id-alt',
  );
  register_post_type( 'clients', $args );
}

add_action( 'init', 'clients' );
