<?php

/*
* Template Name: Services
*/

get_header();

?>


<main id="services">


  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="content container">

      <h1><?php echo get_the_title(); ?></h1>

      <?php the_content(); ?>

    </div>

  <?php endwhile; endif; ?>

</main>

<?php get_footer(); ?>
