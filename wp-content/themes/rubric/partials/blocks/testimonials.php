<?php
/**
 * Testimonials
 *
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'testimonials-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['align']) ) {
  $className .= ' align' . $block['align'];
}
// Get variables
$title = get_field('title');
?>



<?php
$testimonial = get_field('testimonials');
if( $testimonial ): ?>

  <div class="testimonial">

    <div>

      <span class="user-image">
        <?php
        $image = get_field('testimonial_image',$testimonial);
        if( !empty( $image ) ): ?>
          <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
        <?php endif; ?>
      </span>

      <div>
        <?php echo get_field( 'testimonial_content',  $testimonial ); ?>
        <?php echo get_field( 'testimonial_name',  $testimonial ); ?>
        <?php echo get_field( 'testimonial_job_position',  $testimonial ); ?>
        <?php echo get_field( 'testimonial_company',  $testimonial ); ?>
      </div>

    </div>

  </div>

<?php endif; ?>
