<?php
/**
 * Services
 *
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'services-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['align']) ) {
  $className .= ' align' . $block['align'];
}
// Get variables
$title = get_field('title');
?>

<div id="<?php echo esc_attr($id); ?>" class="services-block">

  <?php
    $posts = get_field('services');

    if( $posts ): ?>
    <ul>
    <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>

      <li>

        <a href="<?php echo get_permalink( $p->ID ); ?>">

          <div>

            <span class="services-icon">
              <?php
              $image = get_field('service_icon',$p->ID);
              if( !empty( $image ) ): ?>
                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
              <?php endif; ?>
            </span>

            <div>
              <span><h5><?php echo get_the_title( $p->ID ); ?></h5></span>
              <div class="service-content"><?php echo get_field( 'service_introduction',  $p->ID )['service_introduction_content']; ?></div>
            </div>

          </div>

        </a>

      </li>
    <?php endforeach; ?>
    </ul>
    <?php endif; ?>


</div>
