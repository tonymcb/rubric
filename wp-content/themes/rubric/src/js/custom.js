jQuery(document).ready(function($) {

  $("#nav-icon").click(function(e) {
      $('#nav-icon').toggleClass('active');
      $('body').toggleClass('fix');
      $('.menu-main-menu-container').toggleClass('active');
      $('.menu-main-menu-container').fadeToggle();
      e.preventDefault();
   });

  $('#home .services-block ul').slick({
    infinite: false,
    arrows: true,
    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
    nextArrow: '<button class="slide-arrow next-arrow"></button>',
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
         fade: true
      }
    }]
  });

  $('#home #testimonials ul').slick({
    dots: true,
    fade: true,
    arrows: false
  });

  $("#banner h1").fitText(0.395);

  if( $(window).width() > 768 ) {

    $("#banner h1").fitText(0.4);

   }

  $(window).resize(function() {

     var width = $(document).width();
     if (width > 768) {

       $("#banner h1").fitText(0.4);

     }

   });

});
