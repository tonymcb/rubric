<?php

/*
* Template Name: Clients
*/

get_header();

?>

<main id="clients">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="content container">

	<h1><?php the_title(); ?></h1>

	<div>
		<?php the_content(); ?>
	</div>


</div>


<div class="clients blue">

	<div class="container">

		<div class="row">

			<div class="col-12">

				<ul>

		    <?php

				  $args = array(
				   'post_type' => 'clients',
				   'posts_per_page' => -1, // limit the number of posts if you like to
				   'orderby' => 'title',
				   'order' => 'ASC'
				  );

				  $custom_query = new WP_Query($args);

				  if ($custom_query->have_posts()) : while($custom_query->have_posts()) : $custom_query->the_post(); ?>

					<li>

						<a href="<?php echo the_permalink(); ?>">

							<div>

						    <div>
						      <?php
						      $image = get_field('client_logo');
						      if( !empty( $image ) ): ?>
						        <span class="client-logo"><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" /></span>
						      <?php endif; ?>
						    </div>

								<h3><?php the_title(); ?></h3>

						    <div class="client-content">
						      <?php the_field('client_content'); ?>
						    </div>

							</div>

						</a>

					</li>


				    <?php the_content();?>

				  <?php endwhile; else : ?>
				    <p>No posts</p>
				  <?php endif; wp_reset_postdata(); ?>

				</ul>

		  </div>

		</div>

	</div>

</div>




<?php if ( comments_open() && ! post_password_required() ) { comments_template( '', true ); } ?>
<?php endwhile; endif; ?>

</main>
<?php get_footer(); ?>
