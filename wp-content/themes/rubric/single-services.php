<?php
/*
Template Name: Services
Template Post Type: post, services
*/
?>
<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php $thumb = get_the_post_thumbnail_url(); ?>



<main id="content">

  <div id="intro" class="container">
    <?php
    $image = get_field('service_icon');
    if( !empty( $image ) ): ?>
      <span class="icon"><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" /></span>
    <?php endif; ?>
    <h1><?php the_title(); ?></h1>
  </div>

<div id="service-content">

  <div class="alignfull blue">

    <div class="container">

    <?php if( have_rows('service_introduction') ): ?>

      <div class="service-introduction row">

        <?php while( have_rows('service_introduction') ): the_row();
          $image = get_sub_field('service_introduction_image');
        ?>

        <div class="col-md-6">

          <div class="content">
             <h2><?php the_sub_field('service_introduction_title'); ?></h2>
             <?php the_sub_field('service_introduction_content'); ?>
          </div>

        </div>


        <div class="col-md-6">

          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />

        </div>

        <?php endwhile; ?>
        <?php endif; ?>

      </div>

    </div>

  </div>

  <section class="container">

    <?php if( have_rows('service_content_block') ): ?>
        <?php while( have_rows('service_content_block') ): the_row();
          $image = get_sub_field('service_content_icon');
        ?>

        <div class="row">

          <div class="col-md-6">

            <div>

              <h2><?php the_sub_field('service_content_title'); ?></h2>
              <?php the_sub_field('service_content_content'); ?>

            </div>

          </div>

          <div class="col-md-6">

            <div class="service-image">
              <div>
                <div>
                  <div>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>

        <?php endwhile; ?>
    <?php endif; ?>

  </section>


</div>

<?php endwhile; endif; ?>

</main>
<?php get_footer(); ?>
